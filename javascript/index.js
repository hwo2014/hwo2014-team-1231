var net = require('net')
  , JSONStream = require('JSONStream')
  , serverHost = process.argv[2]
  , serverPort = process.argv[3]
  , bunyan = require('bunyan')
  , logger = bunyan.createLogger({ name: 'race' })
  , _ = require('lodash')
  , influxDB = require('influx')
  , botName = process.argv[4]
  , botKey = process.argv[5]
  , track = process.argv[6]
  , client = net.connect(serverPort, serverHost, function() {
        return send(
          { msgType: 'join'
          , data:
            { name: botName
            , key: botKey
            }
          }
        )
        return send(
          { msgType: 'joinRace'
          , data:
            { botId:
              { name: botName
              , key: botKey
              }
            , trackName: 'germany'
            , carCount: 1
            }
          }
        )
    }
  )

function degreesToRadians(n) {
  return n * (Math.PI / 180)
}

var influx = influxDB('localhost', 8086, 'tomgco', 'n0mn0mn0m', 'race')

logger.info('I\'m', botName, 'and connect to', serverHost + ':' + serverPort)

client.setTimeout(6000, function () {
  logger.info('Connection timed out')
  process.nextTick(function () {
    process.exit()
  })
})

client.on('connect', function (){
  logger.info('connected')
})

function send(json) {
  client.write(JSON.stringify(json))
  return client.write('\n')
}

var jsonStream = client.pipe(JSONStream.parse())
  , raceInfo = {}
  , tick =
    { tick: 0
    , piece: 0
    , pieceIndex: 0
    , distance: 0
    , angle: 0
    , speed: 0
    , throttle: 0
    }

function pieceRadius(piece, index) {
  var lane = _.find(raceInfo.data.race.track.lanes, { index: index })
    , radius = 0
  if (piece.angle < 0) {
    radius = (piece.radius + lane.distanceFromCenter)
  } else {
    radius = (piece.radius - lane.distanceFromCenter)
  }
  return radius
}

function pieceLength(piece, index) {
  var length = 0

  if (isTurn(piece)) {
    length = (pieceRadius(piece, index)) * degreesToRadians(Math.abs(piece.angle))
  } else {
    length = piece.length
  }

  return length
}

function distanceDelta(currentTick) {
  var delta = 0
    , previousPiece = raceInfo.data.race.track.pieces[tick.pieceIndex]
    , previousDistance = tick.pieceDistance

  if (currentTick.pieceIndex !== tick.pieceIndex) {
    var previousAmountToCarry = previousPiece.length - previousDistance
    delta = previousAmountToCarry + currentTick.pieceDistance
  } else {
    delta = currentTick.pieceDistance - tick.pieceDistance
  }
  return delta
}

function getDataForCar(data) {
  return data.data[0]
}

jsonStream.on('data', function(data) {
  if (data.msgType === 'error') {
    logger.info('error')
  }
  if (data.msgType === 'lapFinished') {
    logger.info('Lap', data.data.lapTime.millis / 1000 + ' s', data.data.car.name)
  }
  // process.stdout.write(JSON.stringify(data) + '\n')
  if (data.msgType === 'crash') {
    logger.info('Crashed')
  } if (data.msgType === 'join') {
    logger.info('Joined')
  } else if (data.msgType === 'gameStart') {
    logger.info('Race started')
  } else if (data.msgType === 'gameEnd') {
    logger.info('Race ended')
    logger.info(data.data.bestLaps[0].result.millis / 1000 + ' s')
  } else if (data.msgType === 'gameInit') {
    logger.info('init', data)
    raceInfo = data
    console.error('http://localhost:1337/#' + raceInfo.gameId)
  }

  if (data.msgType === 'carPositions') {
    var index = data.data[0].piecePosition.pieceIndex
      , pieces = raceInfo.data.race.track.pieces
      , piece = pieces[index]
      , prevTick = _.clone(tick)

    piece.length = pieceLength(piece, getDataForCar(data).piecePosition.lane.startLaneIndex)

    var currentTick =
      { tick: data.gameTick
      , piece: piece
      , pieceIndex: index
      , angle: getDataForCar(data).angle
      , pieceDistance: getDataForCar(data).piecePosition.inPieceDistance
      }

    var delta = distanceDelta(currentTick)

    // find next switch
    if (pieces[index + 1] && pieces[index + 1].switch) {
      var switch2end = pieces.slice(index + 2, pieces.length)
        , position2Start = pieces.slice(0, index)
        , nextSwitch = null
        , prevSwitch = 0

      // find next switch
      switch2end.some(function (piece, i) {
        if (piece.switch) {
          nextSwitch = i
          return true
        }
      })

      // find previous switch
      for(var i = index - 1; i > 0; i--) {
        if (position2Start[i].switch) {
          prevSwitch = i
          // bail out, as we are done.
          i = 0
        }
      }


      var switch2switch = pieces.slice(index + 2, index + 2 + nextSwitch)

      var lanes = raceInfo.data.race.track.lanes
      var shortest = null
      var top = Infinity

      lanes.forEach(function (lane) {
        lane.amount = 0
        lane.radius = 0
        switch2switch.forEach(function (piece) {
          lane.amount += pieceLength(piece, lane.index)
          lane.radius += piece.radius ? pieceRadius(piece, lane.index) : 0
        })
        if (lane.radius < top) {
          top = lane.radius
          shortest = lane.index
        }
      })
      if (getDataForCar(data).piecePosition.lane.endLaneIndex !== shortest && prevTick.throttle < 0.75) {
          send(
            { msgType: 'switchLane'
            , data: (getDataForCar(data).piecePosition.lane.endLaneIndex >= shortest) ? 'Left' : 'Right'
            , tick: data.gameTick
            }
          )
      }
    }

    currentTick.speed = delta / 1

    tick = currentTick
    var t = 0
      , a = Math.abs(tick.angle)
      , pa = Math.abs(prevTick.angle)

    function intoCorner() {
      return (a >= pa)
    }

    function outOfCorner() {
      return !(intoCorner())
    }


    if (isTurnIn(1, index)) {
      t = 0.63
      if (tick.speed >= 7.1) t = 0.35
      else if (Math.abs(piece.angle) < 30) t = 0.9
      else if (tick.speed < 6 && a < 10 && outOfCorner()) t = 0.7
      else if (a > 28 && intoCorner()) t = 0.3
      else if (a < 27 && tick.speed < 5.5 && outOfCorner()) t = 0.731
      else if (a < 20 && a > 7 && tick.speed > 6.5 && outOfCorner()) t = 0.2
      else if (tick.speed >= 7) t = 0.45
    } else if (isTurnIn(2, index)) {
      t = 0.75
      if (tick.speed >= 6 && a === 0) t = 0.65
      else if (tick.speed >= 7) t = 0.45
    } else if (isTurnIn(3, index)) {
      t = 0.79
      if (tick.speed >= 7 && a === 0) t = 0.6
    } else if (isTurnIn(4, index)) {
      t = (prevTick.throttle + 0.1 >= 1.0) ? 1 : prevTick.throttle + 0.1
      if (tick.speed >= 7.5) t = 0.45
    } else if (!isTurnIn(3, index)) {
      t = 1
    } else {
      t = 1
    }
    tick.throttle = (prevTick.throttle + 0.1 >= t) ? t : prevTick.throttle + 0.1
    throttle(tick.throttle)
    influx.writePoint('speed'
    , { time: Date.now()
      , tick: currentTick.tick
      , angle: getDataForCar(data).angle
      , speed: delta / 1 // speed = distance over number of ticks elapsed
      , id: raceInfo.gameId
      , throttle: tick.throttle
      }
    , function (err) {
        if (err) logger.warn(err)
      }
    )

   return
   //if (isTurn([
    // if 1,2 and 3 are straights accelerate
    // if 3 is a turn
    // >> set decelerate
    //
    //return throttle(0.655)
  }

  send(
    { msgType: 'ping'
    , data: {}
    }
  )
})

jsonStream.on('error', function() {
  return logger.info('disconnected')
})

function isTurnIn(n, index) {
  var pieces = raceInfo.data.race.track.pieces
  var array = []
  for (var i = 0; i < n; i++) {
    array[i] = pieces[i + index]
  }
  return isTurn(array)
}

function isTurn(pieces) {
  var hasTurn = false
  if (!Array.isArray(pieces)) pieces = [pieces]
  pieces = pieces.filter(function(e) { return e })
  return pieces.some(function (piece) {
    if (piece.angle !== undefined) {
      hasTurn = true
      return true
    }
  })
}

function throttle(n) {
  influx.writePoint('throttle'
   , { time: Date.now()
     , tick: tick.tick
     , throttle: n
     , id: raceInfo.gameId
     }
   , function (err) {
       if (err) logger.warn(err)
     }
   )

  send(
    { msgType: 'throttle'
    , data: n
    }
  )
}
